<?php

    include('vender/modernways/dialog/src/Model/INotice.php');
    include('vender/modernways/dialog/src/Model/Notice.php');
    include('vender/modernways/dialog/src/Model/INoticeBoard.php');
    include('vender/modernways/dialog/src/Model/NoticeBoard');
    
    $nb = new \ModernWays\Dialog\Model\NoticeBoard();
    $nb->startTimeInKey('test Dialog');
    $nb->setText('Mijn eerste fout');
    $nb->setCaption('dialog component testen');
    $nb->setCode('001');
    $nb->log();
    
    $nb->startTimeInKey('test Dialog 2');
    $nb->setText('Mijn tweede fout');
    $nb->setCaption('dialog component testen');
    $nb->setCode('002');
    $nb->log();
    
    var_dump($nb);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dialog Component</title>
</head>
<body>
    <pre><?php var_dump($nb);?></pre>
</body>
</html>