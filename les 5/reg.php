<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <form action="bevestig.php" method="post">
        <div>
            <label for="voornaam">Voornaam:</label>
            <input type="text" name="voornaam" id="voornaam" />
            
            <label for="familienaam">Familienaam:</label>
            <input type="text" name="familienaam" id="familienaam" />
            
            <label for="aantalpersonen">Aantal personen:</label>
            <input id="aantalpersonen" name="aantalpersonen" type="text" required pattern="^[0-9]">
            
            <label for="geboortedatum">Geboortedatum:</label>
            <input id="geboortedatum" name="geboortedatum" type="text" pattern="$">
            
            <label for="rekeningnummer">Rekeningnummer:</label>
            <input id="rekeningnummer" name="rekeningnummer" type="text"pattern="[a-z]{2}+[0-9]{2}+\.[0-9]+\.[0-9]+\.[0-9]{2,2,4,4,4}$">
            
            <label for="email">Emailadres:</label>
            <input name="email" type="email" placeholder="user@mydomain.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
            
            
            
        </div>
        <button type=submit>Bevestig bestelling</button>
    </form>
</body>
</html>    