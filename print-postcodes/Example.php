<?php
require('../vendor/autoload.php');

class PDF extends FPDF
{
    function LoadData($file)
    {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach($lines as $line)
            $data[] = explode(';',trim($line));
        return $data;
    }
    

    
    function ImprovedTable($header, $data)
    {
        // Column widths
        $prevProv = "Brussel (19 gemeenten)";
        
        $w = array(20, 40, 40, 40, 40);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        
        foreach($data as $row)
        {
            if($row[2] != $prevProv && $row[2] != "")
            {
                
                $this->AddPage();
                $prevProv = $row[2];
                $this->Cell(0,10,$prevProv,0,1);
                $this->Ln();
                
                //Header opnieuw zetten
                for($i=0;$i<count($header);$i++)
                $this->Cell($w[$i],7,$header[$i],1,0,'C');
                $this->Ln();
        
                //Eerste rij nieuwe provincie
                $this->Cell($w[0],6,$row[0],'LR');
                $this->Cell($w[1],6,$row[1],'LR');
                $this->Cell($w[2],6,$row[2],'LR');
                $this->Cell($w[3],6,$row[3],'LR');
                $this->Cell($w[4],6,$row[4],'LR');
                $this->Ln();
            }
            else{
                $this->Cell($w[0],6,$row[0],'LR');
                $this->Cell($w[1],6,$row[1],'LR');
                $this->Cell($w[2],6,$row[2],'LR');
                $this->Cell($w[3],6,$row[3],'LR');
                $this->Cell($w[4],6,$row[4],'LR');
                $this->Ln();
            }
           
        }
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
    }
    
    
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    

}

$pdf = new PDF();
$header = array('Postcode', 'Stad', 'Provincie', 'Ville', 'Province');
$data = $pdf->LoadData("postcode.txt");
$pdf->AliasNbPages();
$pdf->SetFont('Arial','I',8);
$pdf->AddPage();
$pdf->ImprovedTable($header, $data);
$pdf->Output();

?>