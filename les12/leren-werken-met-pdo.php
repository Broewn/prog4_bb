
<?php

try{
    $pdo = new PDO('mysql:host=localhost;dbname=brunobuysse;charset=utf8mb4', 'broewn', '');
    echo 'Connectie gemaakt';
    $command = $pdo->query("SELECT * FROM Article");
    $articles = $command->fetchAll(PDO::FETCH_ASSOC);
    
    $command = $pdo->query("call ArticleSelectAll");
    $articlesordered = $command->fetchAll(PDO::FETCH_ASSOC);
    
    $command = $pdo->query("call ArticleSelectOne(1)");
    $articlesone = $command->fetchAll(PDO::FETCH_ASSOC);
}
catch(\PDOException $e){
    echo $e->getMessage();
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Leren werken met PDO</title>
</head>
<body>
    <h1>Ongeordend</h1>
    <pre>
        <?php
        
        var_dump($articles);
        
        ?>
    </pre>
    <h1>geordend</h1>
    <pre>
        <?php
        
        var_dump($articlesordered);
        
        ?>
    </pre>
        <h1>Select one</h1>
    <pre>
        <?php
        
        var_dump($articlesone);
        
        ?>
    </pre>
</body>
</html>