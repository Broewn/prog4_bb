<?php
namespace Programmeren4\Article\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null){
        parent::__construct($route, $noticeBoard);
        $this->noticeBoard->startTimeInKey('PDO Connection');
        try {
            $this->pdo = new \PDO('mysql:host=localhost;dbname=brunobuysse;charset=utf8', 'broewn', '');
            $this->noticeBoard->setText('PDO connectie gelukt!');
            $this->noticeBoard->setCaption('PDO connectie voor Article');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} in bestand {$e->getFile()}");
            $this->noticeBoard->setCaption('PDO connectie voor Article');
            $this->noticeBoard->setCode($e->getCode());
        }
        $this->noticeBoard->log(); 
        // create een instantie van een prikbord om validatiefouten
        // op te plaatsen
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Programmeren4\Article\Model\Article($modelState);
    } 
    
    
    public function editing()
    {
        if($this->pdo){
            // het model vullen
            $command = $this->pdo->query("call ArticleSelectAll");
            // associatieve array kolomnamen en waarde per rij
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            return $this->view('Home','Editing', $this->model);
        }
        else return $this->view('Home', 'Error', null);
    }
    
    public function inserting(){
        return $this->view('Home', 'Inserting', null);
    }
    public function insert() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        // steekt de invulvelden in het model
        $this->model->setName(filter_input(INPUT_POST, 'ArticleName', FILTER_SANITIZE_STRING));
        $this->model->setPurchaseDate(filter_input(INPUT_POST, 'ArticlePurchaseDate', FILTER_SANITIZE_STRING));
        $this->model->setPrice(filter_input(INPUT_POST, 'ArticlePrice', FILTER_SANITIZE_STRING));
        // controlleert ofdat alles aan de eisen voldoet en schrijft het naar de database
        if($this->model->isValid()) {
            $statement = $this->pdo->prepare("Call ArticleInsert(:pName, :pDate, :pPrice, @pId)");
            // steekt de waarden van de invulvelden in een model en stuurt ze meet met de stored procedure.
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $statement->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_INT);
            $statement->execute();
            $this->model->setId($this->pdo->query('SELECT @pId')->fetchColumn());
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             * Als ik nu mijn pagina herlaad ga ik geen dubbele inserts krijgen.
             */
            header("Location: /Article-test.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Inserting', $this->model);
        }
    }
    
    public function updating() {
        // derde parameter in het pad, is meestal een id
        if ($this->pdo){
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call ArticleSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setPurchaseDate($articleOne['PurchaseDate']);
            $this->model->setPrice($articleOne['Price']);
            return $this->view('Home','Updating', $this->model);
        }
        else{
            return $this->view('Home', 'Error', null);
        }
    }
    
    public function update(){
           if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        $this->model->setName(filter_input(INPUT_POST, 'ArticleName', FILTER_SANITIZE_STRING));
        $this->model->setPurchaseDate(filter_input(INPUT_POST, 'ArticlePurchaseDate', FILTER_SANITIZE_STRING));
        $this->model->setPrice(filter_input(INPUT_POST, 'ArticlePrice', FILTER_SANITIZE_STRING));
        $this->model->setId(filter_input(INPUT_POST, 'ArticleId', FILTER_SANITIZE_NUMBER_INT));
        
        if($this->model->isValid()) {
            $sth = $this->pdo->prepare("CALL ArticleUpdate(:pName, :pDate, :pPrice, :pId)");
            $sth->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $sth->bindValue(':pDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $sth->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_INT);
            $sth->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $sth->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            header("Location: /Article-test.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Updating', $this->model);
        }
    }
    
    public function deleting() {
       
        if($this->pdo) {
            // zoek op basis van het id, om vervolgens het te verwijderen artikel weer te geven
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("Call ArticleSelectOne(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            $array = $statement->fetch(\PDO::FETCH_ASSOC);
            
            $this->model->setName($array['Name']);
            $this->model->setPurchaseDate($array['PurchaseDate']);
            $this->model->setPrice($array['Price']);
    
            return $this->view('Home', 'Deleting', $this->model);
        } else {
            return $this->view('Home', 'Error');
        }
    }
   
   // artikel verwijderen
   public function del() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        $this->model->setId(filter_input(INPUT_POST, 'ArticleId', FILTER_SANITIZE_NUMBER_INT));
        if($this->pdo) {
            $statement = $this->pdo->prepare("Call ArticleDelete(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            header("Location: /Article-test.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Updating', $this->model);
        }
    }

}